import { of } from 'rxjs';

export function getMocks() {
  const mocks = {
    restangular: {
      one: () => {},
      all: () => {}
    },
    dialogRef: {
      afterClosed: () => of(true),
      file: '',
      onDelete: of({}),
      onRename: of({})
    },
    matDialogData: {

    },
    dialog: {
      open: () => mocks.dialogRef,
      afterClosed: () => of({})
    },
    fileManagerConfig: {
      baseUrl: 'http://ngxfilemanager',
      deleteUrl: () => '/url/delete',
      renameUrl: () => '/url/rename',
      uploadUrl: () => '/url/upload',
      listUrl: () => '/url/list',
      config: {
        interface: {
          fileSelectCallback: () => {
            return {};
          }
        }
      }
    },
    contentService: {
      rename: () => of({}),
      delete: () => of({}),
      all: () => of([]),
    },
    snackBar: {
      open: () => {}
    }
  };
  return mocks;
}
