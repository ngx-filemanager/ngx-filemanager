import { TestBed, inject } from '@angular/core/testing';

import { ContentIconsDictionary, ContentIcons } from './content-type-mapping';
import { async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';

describe('ContentTypeMapping', () => {

  it('should jpeg be mapped to image icon', async(() => {
    expect('fa-file-image').toEqual(ContentIconsDictionary['image/jpeg']);
  }));

  it('should png be mapped to image icon', async(() => {
    expect('fa-file-image').toEqual(ContentIconsDictionary['image/png']);
  }));

  it('should gif be mapped to image icon', async(() => {
    expect('fa-file-image').toEqual(ContentIconsDictionary['image/gif']);
  }));

  it('should jpg be mapped to image icon', async(() => {
    expect('fa-file-image').toEqual(ContentIconsDictionary['image/jpg']);
  }));

  it('should svg be mapped to image icon', async(() => {
    expect('fa-file-image').toEqual(ContentIconsDictionary['image/svg']);
  }));

  it('should svg+xml  be mapped to image icon', async(() => {
    expect('fa-file-image').toEqual(ContentIconsDictionary['image/svg+xml']);
  }));

  it('should html be mapped to html icon', async(() => {
    expect('fa-code').toEqual(ContentIconsDictionary['text/html']);
  }));

  it('should pdf be mapped to pdf icon', async(() => {
    expect('fa-file-pdf').toEqual(ContentIconsDictionary['application/pdf']);
  }));

  it('should x-log be mapped to text icon', async(() => {
    expect('fa-file-alt').toEqual(ContentIconsDictionary['text/x-log']);
  }));

  it('should plain text be mapped to text icon', async(() => {
    expect('fa-file-alt').toEqual(ContentIconsDictionary['text/plain']);
  }));

  it('should spreadsheet be mapped to spreadsheet icon', async(() => {
    expect('fa-file-excel').toEqual(ContentIconsDictionary['application/vnd.oasis.opendocument.spreadsheet']);
  }));

});
