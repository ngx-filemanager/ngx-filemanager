export const ContentIcons = {
  Image: 'fa-file-image',
  PDF: 'fa-file-pdf',
  Text: 'fa-file-alt',
  Spreadsheet: 'fa-file-excel',
  HTML: 'fa-code',
  Default: 'fa-file',
  Folder: 'fa-folder',
  LevelUp: 'fa-arrow-left'
};


export const ContentIconsDictionary: { [contentType: string]: string } = {
  'image/jpeg': ContentIcons.Image,
  'image/png': ContentIcons.Image,
  'image/gif': ContentIcons.Image,
  'image/jpg': ContentIcons.Image,
  'text/html': ContentIcons.HTML,
  'image/svg': ContentIcons.Image,
  'image/svg+xml': ContentIcons.Image,
  'application/pdf': ContentIcons.PDF,
  'text/x-log': ContentIcons.Text,
  'text/plain': ContentIcons.Text,
  'application/vnd.oasis.opendocument.spreadsheet': ContentIcons.Spreadsheet
};

export const ContentModelDictionary = {
  mime_type: 'mimeType',
  parent_id: 'parentId',
  external_link: 'externalLink',
  public_filename: 'publicFilename',
};
