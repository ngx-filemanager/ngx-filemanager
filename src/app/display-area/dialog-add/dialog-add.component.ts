import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Content } from 'src/app/models/content.model';
import * as _ from 'lodash';
import { FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { FileManagerConfig } from 'src/app/app.config';

@Component({
  selector: 'app-dialog-add',
  templateUrl: './dialog-add.component.html',
  styleUrls: ['./dialog-add.component.scss']

})
export class DialogAddComponent {
  content: Content;
  uploader: FileUploader;

  constructor(
    public dialogRef: MatDialogRef<DialogAddComponent>,
    private fileManagerConfig: FileManagerConfig,
  ) {
    this.uploader = new FileUploader({
      authToken: this.fileManagerConfig.privateToken,
      url: this.fileManagerConfig.uploadUrl,
    });
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);

  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    const error = JSON.parse(response);
    console.log(error);
}
}
