import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { FileManagerConfig } from '../../app.config';
import * as helpers from '../../../spec/helpers';

import { DialogAddComponent } from './dialog-add.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('DialogAddComponent', () => {
  let component: DialogAddComponent;
  let fixture: ComponentFixture<DialogAddComponent>;
  const mocks = helpers.getMocks();

  beforeEach(async(() => {
    spyOn(mocks.fileManagerConfig, 'deleteUrl').and.callThrough();
    spyOn(mocks.snackBar, 'open').and.callThrough();

    TestBed.configureTestingModule({
      declarations: [DialogAddComponent],
      providers: [
        { provide: MatDialogRef, useValue: mocks.dialogRef },
        { provide: MAT_DIALOG_DATA, useValue: mocks.matDialogData },
        { provide: FileManagerConfig, useValue: mocks.fileManagerConfig }
      ],
      imports: [TranslateModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
    fixture = TestBed.createComponent(DialogAddComponent);
    component = fixture.componentInstance;
  }));

  it('display add dialog', () => {
    expect(fixture.debugElement.queryAll(By.css('h1')).length).toEqual(1);
  });
});
