import { CUSTOM_ELEMENTS_SCHEMA, Renderer2 } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DisplayAreaComponent } from './display-area.component';
import { ActivatedRoute } from '@angular/router';
import { FileManagerConfig, ContentType} from '../app.config';
import { ContentIcons } from '../content-type-mapping';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../spec/helpers';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Content } from '../models/content.model';
import { ContentService } from '../services/content.service';
import { of } from 'rxjs';

describe('DisplayAreaComponent', () => {

  let component: DisplayAreaComponent;
  let fixture: ComponentFixture<DisplayAreaComponent>;
  const mocks = helpers.getMocks();

  beforeEach(async(() => {
    spyOn(mocks.fileManagerConfig, 'listUrl').and.callThrough();

    TestBed.configureTestingModule({
        declarations: [ DisplayAreaComponent],
        providers: [ { provide: FileManagerConfig, useValue: mocks.fileManagerConfig},
                    { provide: ActivatedRoute, useValue: {}},
                    { provide: ContentService, useValue: mocks.contentService },
                    { provide: Renderer2, useValue: {}},
                    { provide: MatDialog, useValue: mocks.dialog },
                    { provide: MatSnackBar, useValue: mocks.snackBar }
        ],
        imports: [TranslateModule.forRoot()],
        schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });

    fixture = TestBed.createComponent(DisplayAreaComponent);
    component = fixture.componentInstance;
  }));

  it('should call buildBackButton on initialization', () => {
    spyOn(component, 'buildBackButton').and.callThrough();
    component.ngOnInit();
    expect(component.buildBackButton).toHaveBeenCalled();
  });

  it('should put current path in previous path', () => {
    component.currentPath = '123';
    expect(component.previousPaths).toEqual([]);
    component.getContent('');
    expect(component.previousPaths).toEqual(['123']);
  });

  it('should define current path as parent_id', () => {
    component.currentPath = '456';
    expect(component.currentPath).toEqual('456');
    component.getContent('123');
    expect(component.currentPath).toEqual('123');
  });

  it('the contents be defined as the returned content of api', () => {
    const c1 = new Content({id: 1, name: 'some'});
    c1.refreshFields();
    const c2 = new Content({id: 1, name: 'some'});
    c2.refreshFields();
    const apiContent = [c1, c2];
    const contentService = TestBed.get(ContentService);
    spyOn(contentService, 'all').and.returnValue(of(apiContent));
    expect(component.contents).toEqual([]);
    component.getContent('');
    expect(component.contents).toEqual(apiContent);
  });

  it('should call buildBackButton on getContent with parent as parameter', () => {
    spyOn(component, 'buildBackButton').and.callThrough();
    component.getContent('123');
    expect(component.buildBackButton).toHaveBeenCalled();
  });

  it('should not call buildBackButton on getContent with no parent as parameter', () => {
    spyOn(component, 'buildBackButton').and.callThrough();
    component.getContent('');
    expect(component.buildBackButton).not.toHaveBeenCalled();
  });

  it('should the back button update the parent', () => {
    expect(component.contents).toEqual([]);
    const currentPath = component.currentPath;
    component.getContent('123');
    expect(component.contents[0].icon).toEqual(ContentIcons.LevelUp);
    expect(component.contents[0].parentId).toEqual(currentPath);
  });

  it('should build back button with name back', () => {
    component.buildBackButton();
    expect(component.backNavigationFolder.name).toEqual('button.back');
  });

  it('should build back button with mimeType application/folder', () => {
    component.buildBackButton();
    expect(component.backNavigationFolder.mimeType).toEqual('application/folder');
  });

  it('should build back button with parent_id empty', () => {
    component.buildBackButton();
    expect(component.backNavigationFolder.parentId).toEqual('');
  });

  it('should build back button with filename go up', () => {
    component.buildBackButton();
    expect(component.backNavigationFolder.filename).toEqual('go up');
  });

  it('should build back button with is_image false', () => {
    component.buildBackButton();
    expect(component.backNavigationFolder.isImage).toEqual(false);
  });

  it('should build back button with type folder', () => {
    component.buildBackButton();
    expect(component.backNavigationFolder.type).toEqual(ContentType.Folder);
  });

  it('should build back button with icon level up', () => {
    component.buildBackButton();
    expect(component.backNavigationFolder.icon).toEqual(ContentIcons.LevelUp);
  });

  it('should call fileSelectCallback in send', () => {
    component.selectedContent = {publicFilename: 'bli'} as Content;
    const fileManagerConfig = TestBed.get(FileManagerConfig);
    spyOn(fileManagerConfig.config.interface, 'fileSelectCallback').and.returnValue({});
    component.send();
    expect(fileManagerConfig.config.interface.fileSelectCallback).toHaveBeenCalled();
  });

  it('should call dialog open after openDelete', () => {
    const dialog = TestBed.get(MatDialog);
    spyOn(dialog, 'open').and.callThrough();
    component.openDelete();
    expect(dialog.open).toHaveBeenCalled();
  });

  it('should call dialog open after openRename', () => {
    const dialog = TestBed.get(MatDialog);
    spyOn(dialog, 'open').and.callThrough();
    component.openRename();
    expect(dialog.open).toHaveBeenCalled();
  });

  it('should get the content of the folder', () => {
    spyOn(component, 'getContent').and.callThrough();
    const file = new Content();
    file.id = '1';
    file.type = ContentType.Folder;
    component.openContent(file);
    expect(component.getContent).toHaveBeenCalled();
  });

  it('should not get the content if the file passed is not a folder', () => {
    spyOn(component, 'getContent').and.callThrough();
    const file = new Content();
    file.id = '1';
    file.type = ContentType.File;
    component.openContent(file);
    expect(component.getContent).not.toHaveBeenCalled();
  });

  it('should get the content of the parent\'s file if the file is the level up', () => {
    spyOn(component, 'getContent').and.callThrough();
    const file = new Content();
    file.id = '1';
    file.type = ContentType.Folder;
    file.icon =  ContentIcons.LevelUp;
    file.parentId = '2';
    component.openContent(file);
    expect(component.getContent).toHaveBeenCalledWith('2');
  });

  it('should get the content if the file is not the level up', () => {
    spyOn(component, 'getContent').and.callThrough();
    const file = new Content();
    file.id = '1';
    file.icon = ContentIcons.Folder;
    file.type = ContentType.Folder;
    component.openContent(file);
    expect(component.getContent).toHaveBeenCalledWith('1');
  });

  it('should pop previus path if the file is the level up', () => {
    spyOn(component, 'getContent').and.callThrough();
    const file = new Content();
    file.id = '1';
    file.type = ContentType.Folder;
    file.icon = ContentIcons.LevelUp;
    component.previousPaths = [1,2,3];
    component.openContent(file);
    expect(component.previousPaths).toEqual([1, 2]);
  });

});
