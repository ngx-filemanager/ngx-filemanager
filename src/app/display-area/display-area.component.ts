import { Component, OnInit, Renderer2 } from '@angular/core';
import { ContentIcons } from '../content-type-mapping';
import { MatDialog, MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import { FileManagerConfig, ContentType } from '../app.config';
import { Content } from 'src/app/models/content.model';
import { ContentService } from '../services/content.service';
import { DialogRenameComponent } from './dialog-rename/dialog-rename.component';
import { DialogDeleteComponent } from './dialog-delete/dialog-delete.component';
import { FileUploader } from 'ng2-file-upload';

import * as _ from 'lodash';
import { DialogAddComponent } from './dialog-add/dialog-add.component';

@Component({
  selector: 'app-display-area',
  templateUrl: './display-area.component.html',
  styleUrls: ['./display-area.component.scss']
})
export class DisplayAreaComponent implements OnInit {
  selectedContent: Content;
  contents: Content[];
  currentPath = '';
  previousPaths = [];
  backNavigationFolder: Content;

  constructor(
    private fileManagerConfig: FileManagerConfig,
    private dialog: MatDialog,
    private contentService: ContentService,
    private translateService: TranslateService,
    private snackBar: MatSnackBar
  ) {
    this.contents = [];
  }
  ngOnInit() {
    this.getContent('');
    this.buildBackButton();
  }

  getContent(parentId: string) {
    this.contentService.all(parentId).subscribe(contents => {
      this.previousPaths = _.union(this.previousPaths, [this.currentPath]);
      this.currentPath = parentId;

      this.contents = [];

      contents.forEach(content => {
        content = Object.assign(new Content(), content);
        content.refreshFields();
        this.contents.push(content);
      });

      if (parentId !== '') {
        this.buildBackButton();
        this.contents.unshift(this.backNavigationFolder);
      }
    });
  }

  buildBackButton() {
    let parenId = this.previousPaths[this.previousPaths.length - 1];
    if (!parenId) {
      parenId = '';
    }

    const backButtonFields = {
      name: this.translateService.instant('button.back'),
      parentId: parenId,
      mimeType: 'application/folder',
      filename: 'go up',
      externalLink: null,
      isImage: false,
      type: ContentType.Folder,
      icon: ContentIcons.LevelUp
    };

    this.backNavigationFolder = Object.assign(new Content(), backButtonFields);
  }

  openContent(content: Content) {
    this.selectedContent = undefined;
    if (content.typeLowerCase() === ContentType.Folder.toLowerCase()) {
      if (content.icon === ContentIcons.LevelUp) {
        this.previousPaths.pop();
        this.currentPath =
          this.previousPaths.length > 0
            ? this.previousPaths[this.previousPaths.length - 1]
            : '';
        this.getContent(content.parentId);
      } else {
        this.getContent(content.id);
      }
    }
  }

  selectContent(content: Content) {
    this.selectedContent = content;
  }

  isContentSelected(): boolean {
    return (!_.isNil(this.selectedContent) && (this.selectedContent.icon !== ContentIcons.LevelUp));
  }

  send() {
    this.fileManagerConfig.config.interface.fileSelectCallback(
      this.fileManagerConfig.baseUrl + this.selectedContent.publicFilename
    );
  }

  openAdd() {
    const dialogRef = this.dialog.open(DialogAddComponent, {
      width: '800px'
    });

    dialogRef.afterClosed().subscribe((content: Content) => {
      this.getContent('');
    });
  }

  openDelete() {
    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '300px',
      data: this.selectedContent
    });

    dialogRef.afterClosed().subscribe((content: Content) => {
      if (_.isNil(content)) {
        return null;
      }
      content = Object.assign(new Content(), content);
      content.refreshFields();

      this.contentService.delete(content).subscribe(
        (remoteContent: Content) => {
          _.remove(this.contents, ['id', content.id]);
          this.displayMsg(
            'dialog.delete.' + content.typeLowerCase() + '.success'
          );
        },
        error => {
          console.log(error);
          this.displayMsg(
            'dialog.delete.' + content.typeLowerCase() + '.error'
          );
        }
      );
    });
  }

  openRename() {
    const dialogRef = this.dialog.open(DialogRenameComponent, {
      width: '300px',
      data: this.selectedContent
    });
    dialogRef.afterClosed().subscribe((content: Content) => {
      if (_.isNil(content)) {
        return null;
      }
      content = Object.assign(new Content(), content);
      content.refreshFields();
      this.contentService.rename(content).subscribe(
        (remoteContent: Content) => {
          const index = this.contents.findIndex(
            localContent => localContent.id === content.id
          );
          if (_.isNil(remoteContent)) {
            this.contents[index] = content;
          } else {
            remoteContent = Object.assign(new Content(), remoteContent);
            remoteContent.refreshFields();
            this.contents[index] = remoteContent;
          }
          this.displayMsg(
            'dialog.rename.' + content.typeLowerCase() + '.success'
          );
        },
        error => {
          console.log(error);
          this.displayMsg('dialog.rename.' + content.typeLowerCase() + '.error');
        }
      );
    });
  }

  displayMsg(msg: string) {
    this.snackBar.open(this.translateService.instant(msg), null, {
      duration: 2000
    });
  }
}
