import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { FileManagerConfig } from '../../app.config';
import * as helpers from '../../../spec/helpers';

import { DialogRenameComponent } from './dialog-rename.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormsModule } from '@angular/forms';

describe('DialogRenameComponent', () => {
  let component: DialogRenameComponent;
  let fixture: ComponentFixture<DialogRenameComponent>;
  const mocks = helpers.getMocks();

  beforeEach(async(() => {
    spyOn(mocks.fileManagerConfig, 'deleteUrl').and.callThrough();
    spyOn(mocks.snackBar, 'open').and.callThrough();

    TestBed.configureTestingModule({
      declarations: [DialogRenameComponent],
      providers: [
        { provide: MatDialogRef, useValue: mocks.dialogRef },
        { provide: MAT_DIALOG_DATA, useValue: mocks.matDialogData },
        { provide: FileManagerConfig, useValue: mocks.fileManagerConfig }
      ],
      imports: [TranslateModule.forRoot(), FormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
    fixture = TestBed.createComponent(DialogRenameComponent);
    component = fixture.componentInstance;
  }));

  it('display rename dialog', () => {
    expect(fixture.debugElement.queryAll(By.css('h1')).length).toEqual(1);
  });
});
