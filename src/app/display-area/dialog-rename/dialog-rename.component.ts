import { Component, Inject } from '@angular/core';
import { Content } from 'src/app/models/content.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as _ from 'lodash';

@Component({
  selector: 'app-dialog-rename',
  templateUrl: './dialog-rename.component.html'
})
export class DialogRenameComponent {
  content: Content;

  constructor(
    public dialogRef: MatDialogRef<DialogRenameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Content
  ) {
    this.content = _.clone(data);
  }
}
