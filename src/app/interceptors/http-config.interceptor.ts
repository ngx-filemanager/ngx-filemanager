import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import { FileManagerConfig } from '../app.config';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  rota: string;

  constructor(private fileManagerConfig: FileManagerConfig) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = this.fileManagerConfig.privateToken;
    if (token) {
      request = request.clone({
        headers: request.headers
          .set('Authorization', token)
          .set('Private-Token', token)
      });
    }
    return next.handle(request);
  }
}
