import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as _ from 'lodash';
import { Content } from '../models/content.model';
import { FileManagerConfig } from '../app.config';
import { Observable, of } from 'rxjs';
import { isDeleteExpression } from 'typescript';

@Injectable()
export class ContentService {
  content: Content;

  parseParams(params: any): HttpParams {
    const httpParams = new HttpParams({
      fromObject: params
    });
    return httpParams;
  }

  filterContent(content: Content): Content {
    const permitedFields = ['id', 'name'];
    _.difference(_.keys(content), permitedFields).forEach(key => {
      delete content[key];
    });
    return content;
  }

  constructor(protected http: HttpClient, private fileManagerConfig: FileManagerConfig) {}

  rename(content: Content): Observable<Content> {
    const url = this.fileManagerConfig.baseUrl + this.fileManagerConfig.renameUrl + '/' + content.id;
    const params = {};
    params[this.fileManagerConfig.apiRootKey] = this.filterContent(_.clone(content));
    return this.http.post<Content>(url, params);
  }

  upload(base64ImagesJson: any): Observable<Content> {
    const url = this.fileManagerConfig.baseUrl + this.fileManagerConfig.uploadUrl;
    // const params = {};
    // params['file'] = base64ImagesJson;
    return this.http.post<Content>(url, {file: base64ImagesJson});
  }

  delete(content: Content): Observable<Content> {
    const url = this.fileManagerConfig.baseUrl + this.fileManagerConfig.deleteUrl + '/' + content.id;
    return this.http.delete<Content>(url);
  }

  all(parentId: string|number): Observable<Content[]> {
    return this.http.get<Content[]>(this.fileManagerConfig.baseUrl + this.fileManagerConfig.listUrl + parentId);
  }

}
