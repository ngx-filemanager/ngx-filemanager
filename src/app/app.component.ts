import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngx-filemanager';

  constructor(translate: TranslateService) {
    translate.setDefaultLang(translate.getBrowserLang() || 'en');
    translate.use(translate.getBrowserLang());
  }
}
