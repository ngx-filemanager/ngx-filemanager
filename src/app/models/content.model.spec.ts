import { async } from '@angular/core/testing';
import { Content } from './content.model';

describe('Content Model', () => {

  it('should jpeg be mapped to image icon', async(() => {
    const content =  new Content({id: '1'});
    expect(content.id).toEqual('1');
  }));


  // FIXME make this test
  // it('define the file icon based on file type', () => {
  //   let file = {id: 1, type: ContentType.File, mimeType: 'image/jpg'};
  //   let apiContent = [file];
  //   allReturn = {getList: () => {return Observable.of(apiContent)}};
  //   restangular.and.returnValue(allReturn);
  //   expect(component.contents).toBeUndefined();
  //   component.getContent('');
  //   expect(component.contents[0].icon).toEqual(ContentIconsDictionary[file.mimeType]);
  // });

  // it('define the folder icon based to file', () => {
  //   let file = {id: 1, type: ContentType.Folder};
  //   let apiContent = [file];
  //   allReturn = {getList: () => {return Observable.of(apiContent)}};
  //   restangular.and.returnValue(allReturn);
  //   expect(component.contents).toBeUndefined();
  //   component.getContent('');
  //   expect(component.contents[0].icon).toEqual(ContentIcons.Folder);
  // });
});
