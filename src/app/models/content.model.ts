import { ContentType } from '../app.config';
import * as _ from 'lodash';
import {
  ContentModelDictionary,
  ContentIconsDictionary,
  ContentIcons
} from '../content-type-mapping';

export class Content {
  id: string;
  name: string;
  parentId?: string;
  mimeType?: string;
  filename?: string;
  externalLink?: string;
  isImage?: boolean;
  type?: string;
  icon?: string;
  publicFilename?: string;

  constructor(params?: any) {
    if (params && params.id) {
      this.id = params.id;
    }

    if (params && params.name) {
      this.name = params.name;
    }
  }

  typeLowerCase(): string {
    return this.type.toLowerCase() === ContentType.Folder.toLowerCase()
      ? 'folder'
      : 'file';
  }

  refreshFields() {
    _.keys(ContentModelDictionary).forEach(element => {
      this[ContentModelDictionary[element]] = this[element];
    });
    switch (this.type) {
      case ContentType.File:
        this.icon =
          ContentIconsDictionary[this.mimeType] || ContentIcons.Default;
        break;
      case ContentType.Folder:
        this.icon = ContentIcons.Folder;
        break;
      default:
        this.type = ContentType.File;
        this.icon = ContentIcons.Default;
        break;
    }
  }
}
