import { Injectable } from '@angular/core';

// FIXME make theses contants generic
export const ContentType = {
    File: 'UploadedFile',
    Folder: 'Folder'
};

export interface ConfigFileManager {
    baseUrl: string;
    listUrl: string;
    uploadUrl: string;
    renameUrl: string;
    deleteUrl: string;
    privateToken: string;
    fileObjectTransformer?: (any) => any;
    apiRootKey: string;
    interface: FileManagerInterface;
}

@Injectable()
export class FileManagerConfig {

    constructor(public config: ConfigFileManager = {
        baseUrl: '', listUrl: '', uploadUrl: '', renameUrl: '', deleteUrl: '', privateToken: '',
        fileObjectTransformer: (obj: any) => { return obj; }, apiRootKey: '', interface: new FileManagerInterface()
    }) {
    }

    fileObjectTransformer(object: any) {
        if (this.config.fileObjectTransformer === undefined) {
            return object;
        } else {
            return this.config.fileObjectTransformer(object);
        }
    }

    get baseUrl(): string {
        return this.config.baseUrl;
    }

    get listUrl() {
        return this.config.listUrl;
    }

    get uploadUrl() {
        return this.config.uploadUrl;
    }

    get renameUrl() {
        return this.config.renameUrl;
    }

    get deleteUrl() {
        return this.config.deleteUrl;
    }

    get privateToken() {
        return this.config.privateToken;
    }

    get interface() {
        return this.config.interface;
    }

    get apiRootKey(): string {
        return this.config.apiRootKey;
    }

}


export interface IFileManager {
    fileSelectCallback?: (path) => any;
}

@Injectable()
export class FileManagerInterface {
    constructor(public fileManager: IFileManager = {
        fileSelectCallback: (path: any) => { }
    }) {
    }

    fileSelectCallback(path: any) {
        this.fileManager.fileSelectCallback(path);
    }
}
