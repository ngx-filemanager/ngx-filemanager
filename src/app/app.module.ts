import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TreeModule } from 'angular-tree-component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { ManagerComponent } from './manager/manager.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { DisplayAreaComponent } from './display-area/display-area.component';
import {
  FileManagerConfig,
  ContentType,
  FileManagerInterface,
  ConfigFileManager
} from './app.config';

import {
  MatButtonModule,
  MatDialogModule,
  MatToolbarModule,
  MatSnackBarModule,
  MatRadioModule,
  MatCardModule,
  MatProgressBarModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule
} from '@angular/material';
import { ContentService } from './services/content.service';
import { DialogRenameComponent } from './display-area/dialog-rename/dialog-rename.component';
import { DialogDeleteComponent } from './display-area/dialog-delete/dialog-delete.component';
import { FormsModule } from '@angular/forms';
import { HttpConfigInterceptor } from './interceptors/http-config.interceptor';

import { FileUploadModule } from 'ng2-file-upload';
import { DialogAddComponent } from './display-area/dialog-add/dialog-add.component';

const appRoutes: Routes = [
  { path: 'manager', component: ManagerComponent },
  { path: 'navigationbar', component: NavigationBarComponent },
  { path: 'displayarea', component: DisplayAreaComponent },
  {
    path: '',
    redirectTo: 'manager',
    pathMatch: 'full'
  },
  { path: '**', component: DisplayAreaComponent }
];

export function getUrlParam(paramName) {
  const reParam = new RegExp('(?:[?&]|&)' + paramName + '=([^&]+)', 'i');
  const match = window.location.search.match(reParam);

  return match && match.length > 1 ? match[1] : null;
}

export function getFileManagerInterface() {
  if (getUrlParam('editor') === 'CKEditor') {
    const CKEditorFuncNum = getUrlParam('CKEditorFuncNum');
    return new FileManagerInterface({
      fileSelectCallback: path => {
        window.opener.CKEDITOR.tools.callFunction(CKEditorFuncNum, path);
        window.close();
      }
    });
  }
}

export function fileManagerConfig() {
  let localListUrl = '';
  let localBaseUrl = '';
  let localUploadUrl = '';
  let localRenameUrl = '';
  let localDeleteUrl = '';
  let localPrivateToken = '';
  let localApiRootKey = 'root';
  let logInfo = '';
  logInfo = 'Parameters used for filemanager configuration';

  if (window.opener) {
    localListUrl = window.opener.listUrl;
    localBaseUrl = window.opener.baseUrl;
    localUploadUrl = window.opener.uploadUrl;
    localRenameUrl = window.opener.renameUrl;
    localDeleteUrl = window.opener.deleteUrl;
    localPrivateToken = window.opener.privateToken;
    localApiRootKey = window.opener.apiRootKey;
  }
  logInfo += 'List url: ' + localListUrl;
  logInfo += 'Base url: ' + localBaseUrl;
  logInfo += 'Upload url: ' + localUploadUrl;
  logInfo += 'Rename url: ' + localRenameUrl;
  logInfo += 'Dele url: ' + localDeleteUrl;
  logInfo += 'Private Token: ' + localPrivateToken;
  logInfo += 'Api Root Key: ' + localApiRootKey;

  console.log(logInfo);

  return new FileManagerConfig({
    listUrl: localListUrl,
    baseUrl: localBaseUrl,
    uploadUrl: localUploadUrl,
    renameUrl: localRenameUrl,
    deleteUrl: localDeleteUrl,
    privateToken: localPrivateToken,
    apiRootKey: localApiRootKey,
    //FIXME see why this method fileObjectTransformer is needed
    fileObjectTransformer: obj => {
      if (obj.type.toLowerCase() === 'folder') {
        obj.type = ContentType.Folder;
      } else {
        obj.type = ContentType.File;
      }
    },
    interface: getFileManagerInterface()
  } as ConfigFileManager);
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    ManagerComponent,
    DisplayAreaComponent,
    DialogRenameComponent,
    DialogDeleteComponent,
    DialogAddComponent,
    NavigationBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    TreeModule.forRoot(),
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    MatDialogModule,
    MatToolbarModule,
    MatCardModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatInputModule,
    MatRadioModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    FileUploadModule,
    FormsModule,
    AppRoutingModule
  ],
  entryComponents: [DialogRenameComponent, DialogDeleteComponent, DialogAddComponent],
  providers: [
    TranslateService,
    ContentService,
    { provide: FileManagerConfig, useFactory: fileManagerConfig },
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
