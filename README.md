# Ng2Filemanager

[![build status](https://gitlab.com/ngx-filemanager/ngx-filemanager/badges/master/build.svg)](https://gitlab.com/ngx-filemanager/ngx-filemanager/commits/master)
[![coverage report](https://gitlab.com/ngx-filemanager/ngx-filemanager/badges/master/coverage.svg)](https://gitlab.com/ngx-filemanager/ngx-filemanager/commits/master)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.0.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Test Endpoints


## Build with watch
ng build --watch --output-path dist/ngx-filemanager

## bower vaadin upload
bower install

## Integrate with your application
### Angular 1 application
Run the application on port 4200 with ngx-filemanager context. You can run with 'live-server' (npm install -g live-server) or 'http-server' (npm install -g http-server) port 4200, from dist dir to access the application . Ex: http-server -p 4200 -c-1

Proxy requisitions to your angular application with ngx-filemanager context to the server running on port 4200.

Pass these attributes throught javascrit 
```
window.uploadUrl = '/api/v1'
window.listUrl = '/api/v1';
window.privateToken = 'c39kdo3902j3902j0md2380';
```
